import sqlite3

class BD:
    Id= ''
    Nombres= ''
    Correo=''
    Carrera=''
    Ciclo=''
        
    def __init__(self):
        self.conexion = sqlite3.connect('registro.sqlite3')
        print ('Conexión establecida')
        self.cursor= None

    def basedata(self):
        datos ='''CREATE TABLE Datos (
            Id INTEGER PRIMARY KEY UNIQUE, 
            Nombres TEXT,
            Correo TEXT,
            Carrera TEXT,
            Ciclo TEXT)'''

        self.cur = self.conexion.cursor()
        self.cur.execute(datos)
        print ('Base de datos creada')
        self.cur.close()

    def matricula(self):
        print ('Completa los datos para registrar la matriula')
        Id= input('Ingrese la identificacion:\t')
        Nombres= input('Ingrese los Nombres completos:\t')
        Correo= input('Ingrese el correo:\t')
        Carrera= input('Ingrese la Carrera:\t')
        Ciclo= input('Ingrese el ciclo:\t')

        self.cur = self.conexion.cursor()
        self.cur.executemany('INSERT INTO Datos (Id, Nombres, Correo, Carrera, Ciclo) VALUES (?,?,?,?,?)',[(Id, Nombres, Correo, Carrera, Ciclo)])
        self.conexion.commit()
        self.conexion.close()
        print ('Matricula exitosa')
        
    def buscar(self):
        Id= str(input('Ingrese el id del estudiante:\t'))
        
        self.cur= self.conexion.cursor()
        self.cur.execute('SELECT * FROM Datos WHERE id = (?)', (Id,))
        fila= self.cur.fetchall()
        for i in fila:
            print (i)
        self.conexion.close()

    def mostrarregistro(self):
        print ('Estudiantes Matriculados')
        datos = ('SELECT * FROM Datos')
        self.cur= self.conexion.cursor()
        self.cur.execute(datos)
        fila= self.cur.fetchall()
        for i in fila:
            print (i)
        self.conexion.close()

    def borrar(self):
        Id = input(('Ingrese el id del estudiante:\t'))
        self.cur = self.conexion.cursor()
        self.cur.execute('DELETE FROM Datos WHERE Id = (?)', (Id,))
        print ('Estudiante eliminado de la lista')
        self.conexion.commit()
        self.conexion.close()

    def menu(self):
        print('¿Qué proceso desea realizar?')
        print('1 --> Matricular estudiante')
        print('2--> Mostrar registro')
        print('3 --> Buscar estudiante por id')
        print('4 --> Borrar estudiante por id')
        proceso= int(input('Inngrese el número de la accion que desea realizar:\t'))
        if proceso ==1:
            self.matricula()
        elif proceso ==2:
            self.mostrarregistro()
        elif proceso ==3:
            self.buscar()
        elif proceso ==4:
            self.borrar()
        else:
            print('Valor erroneo')
    

if __name__ == '__main__':
    prueba1 = BD()
    #prueba1.basedata()
    #prueba1.menu()
    #prueba1.buscar()
    #prueba1.matricula()
    #prueba1.basedata()
